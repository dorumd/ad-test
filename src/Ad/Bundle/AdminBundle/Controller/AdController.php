<?php
/**
 * Created by PhpStorm.
 * User: dorelmardari
 * Date: 3/17/15
 * Time: 10:35 PM
 */

namespace Ad\Bundle\AdminBundle\Controller;

use Ad\Bundle\AdminBundle\Entity\Ad;
use Ad\Bundle\AdminBundle\Form\AdType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdController
 *
 * @package Ad\Bundle\AdminBundle\Controller
 * @author  Mardari Dorel <mardari.dorua@gmail.com>
 *
 * @Route("/ad")
 */
class AdController extends Controller
{
    /**
     * Ads list in admin panel
     *
     * @Route("", name="ad_index")
     * @Method({"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $ads = $em->getRepository('AdAdminBundle:Ad')->findAll();

        return $this->render(
            'AdAdminBundle:Ad:index.html.twig',
            [
                'ads' => $ads
            ]
        );
    }

    /**
     * Ad create action
     *
     * @param Request $request
     *
     * @Route("/create", name="ad_create")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var Ad $ad */
        $ad = new Ad();

        $form = $this->createForm(new AdType(), $ad, [
            'action' => $this->generateUrl('ad_create'),
            'method' => 'POST'
        ])
            ->add('save', 'submit');

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($ad);
            $em->flush();

            return $this->redirect($this->generateUrl('ad_index'));
        }

        return $this->render(
            'AdAdminBundle:Ad:form.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * Update existing ad
     *
     * @param Ad      $ad
     * @param Request $request
     *
     * @Route("/{id}", name="ad_update")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function updateAction(Ad $ad, Request $request)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new AdType(), $ad, [
            'action' => $this->generateUrl('ad_update', ['id' => $ad->getId()]),
            'method' => 'POST'
        ])
            ->add('save', 'submit');

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($ad);
            $em->flush();

            return $this->redirect($this->generateUrl('ad_index'));
        }

        return $this->render(
            'AdAdminBundle:Ad:form.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * Remove existing ad
     *
     * @param Ad $ad
     *
     * @Route("/delete/{id}", name="ad_remove")
     *
     * @return array
     */
    public function removeAction(Ad $ad)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($ad);
        $em->flush();

        return [];
    }
}