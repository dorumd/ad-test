<?php
/**
 * Created by PhpStorm.
 * User: dorelmardari
 * Date: 3/17/15
 * Time: 10:22 PM
 */

namespace Ad\Bundle\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller
{
    /**
     * Login action
     *
     * @param Request $request
     *
     * @Route("/login", name="security_login")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request)
    {
        /** @var \Symfony\Component\Security\Http\Authentication\AuthenticationUtils $authenticationUtils */
        $authenticationUtils = $this->get('security.authentication_utils');

        // Get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // Get last authentication username
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render(
            'security/login.html.twig',
            [
                'last_username' => $lastUsername,
                'error'         => $error
            ]
        );
    }

    /**
     * Login Check Action
     *
     * @Route("/login_check", name="login_check")
     */
    public function loginCheckAction()
    {
        // This route is handled by Security system
    }


}