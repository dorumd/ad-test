<?php

namespace Ad\Bundle\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Ad
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Ad
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * @var string $image
     * @Assert\File( maxSize = "1024k", mimeTypesMessage = "Please upload a valid Image")
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var string $image
     * @Assert\File( maxSize = "1024k", mimeTypesMessage = "Please upload a valid Image")
     * @ORM\Column(name="secondary_image", type="string", length=255, nullable=true)
     */
    private $secondaryImage;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Ad
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Ad
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Ad
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Ad
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @ORM\PrePersist()
     */
    public function callbackOnCreate()
    {
        $this->setCreated(new \DateTime());
        $this->setUpdated(new \DateTime());
    }

    /**
     * @ORM\PreUpdate()
     */
    public function callbackOnUpdate()
    {
        $this->setUpdated(new \DateTime());
    }

    public function getFullImagePath()
    {
        return null === $this->image ? null : $this->getUploadRootDir().$this->image;
    }

    public function getFullSecondaryImagePath()
    {
        return null === $this->secondaryImage ? null : $this->getUploadRootDir().$this->secondaryImage;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return $this->getTmpUploadRootDir().$this->getId()."/";
    }

    protected function getTmpUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../../web/upload/';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadImage()
    {
        // the file property can be empty if the field is not required
        if (null === $this->image and null === $this->secondaryImage) {
            return;
        }
        if (!$this->id) {
            if ($this->image) {
                $this->image->move($this->getTmpUploadRootDir(), $this->image->getClientOriginalName());
            }
            if ($this->secondaryImage) {
                $this->secondaryImage->move(
                    $this->getTmpUploadRootDir(),
                    $this->secondaryImage->getClientOriginalName()
                );
            }
        } else {
            if ($this->image) {
                $this->image->move($this->getUploadRootDir(), $this->image->getClientOriginalName());
            }
            if ($this->secondaryImage) {
                $this->secondaryImage->move(
                    $this->getUploadRootDir(),
                    $this->secondaryImage->getClientOriginalName()
                );
            }
        }

        if ($this->image) {
            $this->setImage($this->image->getClientOriginalName());
        }

        if ($this->secondaryImage) {
            $this->setSecondaryImage($this->secondaryImage->getClientOriginalName());
        }
    }

    /**
     * @ORM\PostPersist()
     */
    public function moveImage()
    {
        if (null === $this->image and null === $this->secondaryImage) {
            return;
        }

        if (!is_dir($this->getUploadRootDir())) {
            mkdir($this->getUploadRootDir());
        }

        if ($this->image) {
            copy($this->getTmpUploadRootDir().$this->image, $this->getFullImagePath());
            unlink($this->getTmpUploadRootDir().$this->image);
        }

        if ($this->secondaryImage) {
            copy($this->getTmpUploadRootDir().$this->secondaryImage, $this->getFullSecondaryImagePath());
            unlink($this->getTmpUploadRootDir().$this->secondaryImage);
        }
    }

//    /**
//     * @ORM\PreRemove()
//     */
//    public function removeImage()
//    {
//        if ($this->getFullImagePath())
//            unlink($this->getFullImagePath());
//        if ($this->getFullSecondaryImagePath())
//            unlink($this->getFullSecondaryImagePath());
//        rmdir($this->getUploadRootDir());
//    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Ad
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set secondaryImage
     *
     * @param string $secondaryImage
     *
     * @return Ad
     */
    public function setSecondaryImage($secondaryImage)
    {
        $this->secondaryImage = $secondaryImage;

        return $this;
    }

    /**
     * Get secondaryImage
     *
     * @return string
     */
    public function getSecondaryImage()
    {
        return $this->secondaryImage;
    }
}
