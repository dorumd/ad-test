<?php

namespace Ad\Bundle\PublicBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * Index page
     *
     * @Route("", name="default_index")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $ads = $em->getRepository('AdAdminBundle:Ad')->findAll([], ['created' => 'ASC']);

        return $this->render('AdPublicBundle:Default:index.html.twig', ['ads' => $ads]);
    }
}
